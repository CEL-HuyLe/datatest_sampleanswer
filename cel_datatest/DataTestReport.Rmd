---
title: "CEL Datatest Report"
author: "Huy Le"
date: "`r format(Sys.time(), '%d %B, %Y  %H:%M')`"
output: 
  CELRPackage::CELReport:
    number_sections: TRUE
    self_contained: TRUE

knit: (function(inputFile, encoding) {
      out_dir <- '';
      rmarkdown::render("DataTestReport.Rmd",
                        encoding=encoding,
                        output_file=file.path(dirname(inputFile), out_dir, 'DataTestReport.html')) })
---

```{r setup, include=FALSE}
library(CELRPackage)
library(DT)
options(scipen=999)
options(digits=22)
knitr::opts_knit$set(root.dir = normalizePath(".."))
```

```{r, message=FALSE, warning=FALSE, echo=FALSE}
#Data preprocessing
canceled_test <- read_csv("cel_datatest/canceled_test.csv")
sales_test <- read_csv("cel_datatest/sales_test.csv")

names(canceled_test)[names(canceled_test) == 'Order Number'] <-
  'Order No'
names(canceled_test)[names(canceled_test) == 'Line Number'] <-
  'Line No'

canceled_test$`Order No` <- as.factor(canceled_test$`Order No`)
canceled_test$`Line No` <- as.factor(canceled_test$`Line No`)
canceled_test$`Ship To Address No` <- as.factor(canceled_test$`Ship To Address No`)
canceled_test$`Item Code` <- as.factor(canceled_test$`Item Code`)

sales_test$`Order No` <- as.factor(sales_test$`Order No`)
sales_test$`Line No` <- as.factor(sales_test$`Line No`)
sales_test$`Ship To Address No` <- as.factor(sales_test$`Ship To Address No`)
sales_test$`Item Code` <- as.factor(sales_test$`Item Code`)
```

# Summary
```{r,message=FALSE,warning=FALSE,echo=FALSE}
#Data consolidation
Full_data <- canceled_test %>% full_join(sales_test,
                                         by = c(
                                           "Order No",
                                           "Order Date",
                                           "Line No",
                                           "Ship To Address No",
                                           "Item Code"
                                         )) %>%
  replace_na(
    list(
      `Order Qty` = 0,
      `Quantity Shipped` = 0,
      `Quantity Ordered` = 0,
      `Quantity Canceled` = 0
    )
  ) %>%
  mutate(Demand = `Order Qty` + `Quantity Ordered`,
         Fulfillment = `Quantity Shipped`) %>%
  select(
    c(
      "Order No",
      "Order Date",
      "Line No",
      "Ship To Address No",
      "Item Code",
      "Demand",
      "Fulfillment"
    )
  )

SummaryTable <- Full_data %>% summarize(
  `Investigated Period In Days` = max(`Order Date`) - min(`Order Date`),
  `Measured time bucket` = "Daily",
  `Number of Customers` = n_distinct(`Ship To Address No`),
  `Number of SKUs` = n_distinct(`Item Code`),
  `Number of Orders` = n_distinct(`Order No`),
  `Number of Order Lines` = n_distinct(`Order No`, `Line No`),
  `Total Sales Quantity` = sum(Fulfillment),
  `Total Cancelled Quantity` = sum(Demand) -
    sum(Fulfillment)
) %>%
  gather(key = "Measurement", value = "Value")

SummaryTable %>%  datatable(
  rownames = FALSE,
  extensions = list(Buttons = NULL),
  options = list(
    initComplete = JS(
      "function(settings, json) {",
      "$(this.api().table().header()).css({'background-color': '#c94040', 'color': '#fff'});",
      "}"
    ),
    dom = "Blfrtip",
    buttons = list(
      list(
        extend = "collection",
        buttons = c("csv", "excel", "pdf"),
        text = "Download"
      ),
      I("colvis")
    )
  )
) 

```
# Daily Volume

## Sales daily volume
```{r,message=FALSE,warning=FALSE}
sales_volume <-
  sales_test %>% mutate(Month = format(`Order Date`, "%b")) %>%  group_by(`Order Date`, Month) %>% summarize(DailyVolume = sum(`Quantity Shipped`))

BarPlot(
  sales_volume,
  DailyVolume,
  `Order Date`,
  categoryCol = Month,
  title = "Total Daily Sales Volume",
  xAxisLabel = "Order Date",
  yAxisLabel = "Sales Volume (pcs)",
  legendTitle = "Month",
  tooltip = TRUE,
  stacking = "none",
  showLegend = TRUE,
  showLabel = FALSE
) %>%
  hc_colors(c("#CB333B", "gray")) %>%
  hc_plotOptions(column = list(pointWidth = 10)) %>%
  hc_tooltip(pointFormat = "<b>Sales Volume:</b> {point.y}")

```
In general, sales volume in January and February have *__similar pattern__* where the *__seasonality__* of demand is consistently shown through peak seasons and low seasons within each months

## Order daily volume
```{r,message=FALSE,warning=FALSE}
#Data consolidation

Full_data %>% mutate(Month = format(`Order Date`, "%b")) %>%  group_by(`Order Date`, Month) %>% summarize(DailyVolume = sum(`Demand`)) %>% 
BarPlot(
  DailyVolume,
  `Order Date`,
  categoryCol = Month,
  title = "Total Daily Order Volume",
  xAxisLabel = "Order Date",
  yAxisLabel = "Order Volume (pcs)",
  legendTitle = "Month",
  tooltip = TRUE,
  stacking = "none",
  showLegend = TRUE,
  showLabel = FALSE
) %>%
  hc_colors(c("#CB333B", "gray")) %>%
  hc_plotOptions(column = list(pointWidth = 10)) %>%
  hc_tooltip(pointFormat = "<b>Order Volume:</b> {point.y}")
 
```
- In general, the order volume pattern is quite similar to sales volume pattern with approximate size which reveals the fact of high service level. 

# ABC Classification

## For SKUs

### Based on Order Quantity
```{r,message=FALSE,warning=FALSE}
Full_data %>% ABCClassAndContributionPlot(`Item Code`, Demand, abcRange=c(80,95),title="SKUs Classification",valueColTitle="Order Quantity", accPercentColTitle = "Cumulative Contribution") %>% 
   hc_colors(c("#CB333B","gray"))

SKUClassification_OrderQuantity <- Full_data %>% group_by(`Item Code`) %>% summarize(`Order Quantity`=sum(`Demand`)) %>% ABCClassify(`Order Quantity`, abcRange = c(80, 95)) %>% rename("Cumulative Contribution"="accpercentage","ABC Class"="abcclass")

FormatDataTable(SKUClassification_OrderQuantity)

```
- Based on order quantity, 50% total number of SKUs (39/79) are classified as class C.
- Contribution of top SKUs class A (e.g: 10098739, 1040765) is far higher than remaining SKUs.

### Based on Sales Quantity
```{r,message=FALSE,warning=FALSE}
Full_data %>% ABCClassAndContributionPlot(`Item Code`, Fulfillment, abcRange=c(80,95),title="SKUs Classification",valueColTitle="Sales Quantity", accPercentColTitle = "Cumulative Contribution") %>% 
   hc_colors(c("#CB333B","gray"))

SKUClassification_SalesQuantity <- Full_data %>% group_by(`Item Code`) %>% summarize(`Sales Quantity`=sum(`Fulfillment`)) %>% ABCClassify(`Sales Quantity`, abcRange = c(80, 95)) %>% rename("Cumulative Contribution"="accpercentage","ABC Class"="abcclass")

FormatDataTable(SKUClassification_SalesQuantity)

```
- Compared to ABC Classification based on order quantity, ABC Classification based on sales quantity gave similar structure which reveals the fact that there is not much gap in overall service level between class A and class B. 

## For Clients

### Based on Order Quantity
```{r,message=FALSE,warning=FALSE}
Full_data %>% ABCClassAndContributionPlot(`Ship To Address No`, Demand, abcRange=c(80,95),title="Client Classification",valueColTitle="Order Quantity", accPercentColTitle = "Cumulative Contribution") %>% 
   hc_colors(c("#CB333B","gray"))

ClientClassification_OrderQuantity <- Full_data %>% group_by(`Ship To Address No`) %>% summarize(`Order Quantity`=sum(Demand)) %>% ABCClassify(`Order Quantity`, abcRange = c(80, 95)) %>% rename("Cumulative Contribution"="accpercentage","ABC Class"="abcclass")

FormatDataTable(ClientClassification_OrderQuantity)

```
- There are large gap in the number of clients classified into classes. Specifically, the number of clients in class C is 1.5 times the number of clients in class B and 8 times the number of clients in class A.
- Huge number of clients class B and C means that total sales of the business is mostly from few clients class A while the business are spending much time and effort to manage a large number of clients in all classes. From this finding, we can make an assumption about problems on client management and cost management.


### Based on Sales Quantity
```{r,message=FALSE,warning=FALSE}
Full_data %>% ABCClassAndContributionPlot(`Ship To Address No`, Fulfillment, abcRange=c(80,95),title="Client Classification",valueColTitle="Sales Quantity", accPercentColTitle = "Cumulative Contribution") %>% 
   hc_colors(c("#CB333B","gray"))

ClientClassification_SalesQuantity <- Full_data %>% group_by(`Ship To Address No`) %>% summarize(`Sales Quantity`=sum(Fulfillment)) %>% ABCClassify(`Sales Quantity`, abcRange = c(80, 95)) %>% rename("Cumulative Contribution"="accpercentage","ABC Class"="abcclass")

FormatDataTable(ClientClassification_SalesQuantity )
```
- Compared to ABC Classification based on order quantity, in ABC Classification based on sales quantity, the number of client class C mostly remains unchanged while there is small change in the number of client class A and class B. - Specifically, the number of client class A is larger and the number of client class b becomes smaller. This gives us some general signs for the assumption that overall service level of clients class A is lower than this of clients class B.

# Service level
```{r,message=FALSE,warning=FALSE}
#Service level function.
ServiceLevelCalculation <-
  function(data,
           group,
           demandCol,
           fulfillmentCol,
           categoryCol) {
    group <- enquo(group)
    demandCol <- enquo(demandCol)
    fulfillmentCol <- enquo(fulfillmentCol)
    categoryCol <- enquo(categoryCol)
    if (quo_is_null (categoryCol)) {
      Calculation  <-
        data %>% group_by(!!group) %>%
        summarize(Demand = sum(!!demandCol),
                  Fulfillment = sum(!!fulfillmentCol)) %>%
        mutate(`Fill Rate` = Fulfillment / Demand * 100)
    } else{
      categoryCol <- enquo(categoryCol)
      Calculation  <-
        data %>% group_by(!!group, !!categoryCol) %>%
        summarize(Demand = sum(!!demandCol),
                  Fulfillment = sum(!!fulfillmentCol)) %>%
        mutate(`Fill Rate` = Fulfillment / Demand * 100)
    }
    return(Calculation)
  } 
```

## By SKU

```{r,message=FALSE,warning=FALSE}
SKUFillRate <-
  ServiceLevelCalculation(
    data = Full_data,
    group = `Item Code`,
    demandCol = Demand,
    fulfillmentCol = Fulfillment
  ) %>%
  left_join(SKUClassification_OrderQuantity, by = c("Item Code")) %>%
  select(c("Item Code", "ABC Class", "Fill Rate", )) %>%
  replace_na(list(`ABC Class` = "C")) %>% 
  arrange(`ABC Class`, desc(`Fill Rate`))

SKUClassFillRate <- SKUFillRate %>% group_by(`ABC Class`) %>% summarise(`Average Service Level` = mean(`Fill Rate`))
  
FormatDataTable(SKUFillRate)
FormatDataTable(SKUClassFillRate)
```
- The maximum service level achieve is 100%, the minimum value is 0%
- The lowest service level among SKUs Class A is 50%, this figure is 54% for SKUs Class B and 0% for SKUs class C
- 50% number of SKUs achive service level larger than 80% 
- There are small gap in overall service level between class A and class B while this figure drops significantly in class C. 

## By Client

```{r,message=FALSE,warning=FALSE}
ClientFillRate <-
  ServiceLevelCalculation(
    data = Full_data,
    group = `Ship To Address No`,
    demandCol = Demand,
    fulfillmentCol = Fulfillment
  )  %>%
  left_join(ClientClassification_OrderQuantity, by = c("Ship To Address No")) %>%
  select(c("Ship To Address No", "ABC Class", "Fill Rate", )) %>%
  replace_na(list(`ABC Class` = "C"))%>% 
  arrange(`ABC Class`, desc(`Fill Rate`))

ClientClassFillRate <- ClientFillRate %>% group_by(`ABC Class`) %>% summarise(`Average Service Level` = mean(`Fill Rate`))
  
FormatDataTable(ClientFillRate)
FormatDataTable(ClientClassFillRate)
```
- There are some client class A have low service level (0% - 60%), which reveals the risk of lost sales. 
- Generally, performance by client class is better than SKUs class where fill rate of all classes is above 84% and there is not much gap.
- However, the problem is that the class A client has lower performance than class B which highlight the issue on resource allocation of the business.


# Deep-dive Analysis on Class A

## Class A SKUs

```{r,message=FALSE,warning=FALSE}
# Data preparation
SKU_A <-
  Full_data  %>% mutate(Month = format(`Order Date`, "%b")) %>%
  ServiceLevelCalculation(
    group = `Item Code`,
    demandCol = Demand,
    fulfillmentCol = Fulfillment,
    categoryCol = Month
  ) %>%
  left_join(SKUClassification_OrderQuantity, by = c("Item Code")) %>%
  filter(`ABC Class` == "A") %>%
  select(c("Item Code", "Month", "Fill Rate")) %>%
  spread(Month, `Fill Rate`) %>%
  left_join(SKUClassification_OrderQuantity, by = c("Item Code")) %>%
  select(c("Item Code", "Order Quantity", "Jan", "Feb")) %>%
  arrange(desc(`Order Quantity`)) %>%
  mutate(Improvement = Feb - Jan) %>%
  mutate(`Average Fill Rate By SKUs` = sum(Jan, Feb) / 2) %>%
  mutate(Segment = if_else(
    `Average Fill Rate By SKUs` < 90,
    "Less than 90%",
    "Great than or Equal to 90%"
  ))

FormatDataTable(SKU_A)

#Year-to-date Fill Rate
SKU_A %>%  BarPlotSorted(
  yAxisCol=`Average Fill Rate By SKUs`,
  xAxisCol=`Item Code`,
  categoryCol=Segment,
  orderVector = SKU_A$`Item Code`,
  title = "Class A SKUs Fill Rate by Segment",
  xAxisLabel = "Item Code Sorted by Descending Sales Volume",
  yAxisLabel = "Year-to-Date Fill Rate (percent)",
  legendTitle = "Segment",
  stacking = "none",
  showLegend = TRUE
) %>% 
hc_tooltip(pointFormat = "<b>Year-to-Date Fill Rate:</b> {point.y:,.0f}%")
```
- Above chart shows year-to-date fill rate of class A SKUs. Using segment which is determined by threshold 90% and visualization by color, the chart also highlight SKUs whose fill rate are __under target rate__ and need discussion for action plan. Finally, the chart also illustrates the level of importance for each SKUs with the *sorted x-axis by descending order volume*. This will help to suggest the attention level that need to be paid on each SKUs. For example, both item 111931 and item 10135359 have alerting fill rate, but we should concerns more about item 111931 due to higher contribution. Currently, __10/21 SKUs__ is below the target rate.

- In addition, we also need to focus on some SKUs with negative growth (improvement < 0) which needs action taken timely to maintain/improve performance. Currently, __10/21 SKUs__ have negative growth


## Class A Client

```{r,message=FALSE,warning=FALSE}
# Data preparation
Client_A <-
  Full_data  %>% mutate(Month = format(`Order Date`, "%b")) %>%
  ServiceLevelCalculation(
    group = `Ship To Address No`,
    demandCol = Demand,
    fulfillmentCol = Fulfillment,
    categoryCol = Month
  ) %>%
  left_join(ClientClassification_OrderQuantity, by = c("Ship To Address No")) %>%
  filter(`ABC Class` == "A") %>%
  select(c("Ship To Address No", "Month", "Fill Rate")) %>%
  spread(Month, `Fill Rate`) %>%
  mutate(Jan = if_else(is.na(Jan) == 1, Feb, Jan)) %>%
  mutate(Feb = if_else(is.na(Feb) == 1, Jan, Feb)) %>%
  left_join(ClientClassification_OrderQuantity, by = c("Ship To Address No")) %>%
  select(c("Ship To Address No", "Order Quantity", "Jan", "Feb")) %>%
  arrange(desc(`Order Quantity`)) %>%
  mutate(Improvement = Feb - Jan) %>%
  mutate(`Average Fill Rate By Client` = sum(Jan, Feb) / 2) %>%
  mutate(Segment = if_else(
    `Average Fill Rate By Client` < 90,
    "Less than 90%",
    "Great than or Equal to 90%"
  ))

FormatDataTable(Client_A)

#Year-to-date Fill Rate
Client_A %>%  BarPlotSorted(
  yAxisCol=`Average Fill Rate By Client`,
  xAxisCol=`Ship To Address No`,
  categoryCol=Segment,
  orderVector = Client_A$`Ship To Address No`,
  title = "Class A Client Fill Rate by Segment",
  xAxisLabel = "Client Code Sorted by Descending Sales Volume",
  yAxisLabel = "Year-to-Date Fill Rate (percent)",
  legendTitle = "Segment",
  stacking = "none",
  showLegend = TRUE
) %>% 
hc_tooltip(pointFormat = "<b>Year-to-Date Fill Rate:</b> {point.y:,.0f}%")
```

- Above chart shows year-to-date fill rate of class A client. Using segment which is determined by threshold 90% and visualization by color, the chart also highlight clients whose fill rate are __under target rate__ and need discussion for action plan. Finally, the chart also illustrates the level of importance for each client with the *sorted x-axis by descending volume*. This will help to suggest the attention level that need to be paid on each client. For example, both client 1795849 and item 1402387 have alerting fill rate, but we should concerns more about item client 1795849 due to higher contribution. Currently, __28/50 clients__ is below the target rate.

- Client 1795849 whose sales contribution is largest, has the fill rate under the target rate (80%)

- In addition, we also need to focus on some clients with negative growth (improvement < 0) which needs action taken timely to maintain/improve performance. Currently, __25/50 clients__ have negative growth


# Recommendation

## Improving demand planning

As shown in the part 2b, canceled volume has tendency to increase in the peak seasons or just after the peak seasons. It reveals us the issue on sales forecast performance which directly affects the inventory plan. Thus, the demand planning process should be reviewed for further deep-dive analysis to find the gap for improvement. Also, the key insights from part 2a about seasonality of demand may be a factor that need considering for better forecast accuracy.

## Improving inventory policy

The out-of-stock situation usually happens after demand increases dramatically which may be predictable or unpredictable. Thus, an ABC/XYZ classification based on sales contribution and variation coefficient of historical demand should be conducted to design different inventory policies for different product class.

This classification should be conducted not only on single SKUs, but also group of SKUs with similar characteristics. This can help to justify whether clients are willing to switch to another SKUs in the same group if ordered SKUs is out-of-stock. If yes, some techniques relating to pooled risk could be considered in inventory plan for better service level while saving inventory cost for the business.

## Order management

As shown in the part 4b, class A client group have lower performance than class B. For long-term relationship with class A clients, an order management strategy with approriate prority rule for each client should be designed.



